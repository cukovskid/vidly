﻿using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using Vidly.Models;
using Vidly.ViewsModel;
using Vidly.ViewsModels;

namespace Vidly.Controllers
{

    public class MoviesController : Controller
    {

        private ApplicationDbContext _context;

        public MoviesController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        } // po zavrsuvanje da ja zatvori DB






        // GET: Movies
        public ActionResult Index()
        {
          
            var movies = _context.Movies.Include(c => c.Genre).ToList();

            if (movies != null)
            {
                return View(movies);
            }
            return HttpNotFound();
        }


        public ActionResult New()
        { // so ovoj kontroler sakam samo da stignam do Formata

            var genres = _context.Genres.ToList();
            var movieModel = new MovieFormModelView
            {
                Genres = genres
            };

            return View("MovieForm", movieModel);
        }

        public ActionResult Save(Movie movie)
        {// dobieniot Model Movie da go sejvnes kako nov ili update vo DB

            if (movie.Id == 0) // ova Id ke bide 0 bidejki od View ke go pratam kako 0
            {
                movie.DateAdded = DateTime.Now;
                _context.Movies.Add(movie);
            }
            else
            {
                var oldMovie = _context.Movies.Single(m => m.Id == movie.Id);

                oldMovie.Name = movie.Name;
                oldMovie.ReleaseDate = movie.ReleaseDate;
                oldMovie.NumberInStock = movie.NumberInStock;
                oldMovie.GenreId = 2;
                oldMovie.DateAdded = movie.DateAdded;
            }



            _context.SaveChanges();


            return RedirectToAction("Index", "Movies");
        }

        public ActionResult Edit()
        { // so ovoj kontroler sakam samo da stignam do Formata

            var genres = _context.Genres.ToList();
            var movieModel = new MovieFormModelView
            {
                Genres = genres
            };

            return View("MovieForm", movieModel);
        }

        // GET: Movies/Details/{id}
        public ActionResult Details(int id)
        {
            var movie = _context.Movies.Include(c => c.Genre).SingleOrDefault(c => c.Id == id);

            if (movie != null)
            {
                return View(movie);
            }
            return Content("ups");
        }

  


        [Route ("movies/released/{year}/{month:regex(\\d{2}):range(1, 12)}")]
        public ActionResult ByReleaseDate(int year, int month)
        {
            return Content(year + "/" + month);
        }


    }
}