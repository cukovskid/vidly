﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vidly.Models;
using Vidly.ViewsModels;

namespace Vidly.Controllers
{
    public class CustomersController : Controller
    {
        private ApplicationDbContext _contex;

        public CustomersController()
        {
            _contex = new ApplicationDbContext();
        }


        // GET: Customer
        public ActionResult Index()
        {
            var customers = _contex.Customers.Include(c=> c.MembershipType).ToList();
            // Include() se dodava bidejki View ne gi vklucuva i pod klasite 
            return View(customers);
        }

        // GET: Customer
        public ActionResult New()
        {
            var membershipTypes = _contex.MembershipTypes.ToList();
            
            var ViewModel = new NewCustomerViewModel
            {
                MembershipTypes = membershipTypes // prateni samo za DropDown
            };

            // Bidejki samo EDEN model mozam da pratam vo View, ke naptavam Clasa so clasi
            return View("CustomerForm", ViewModel);
        }


        [HttpPost] // tuka doagaat i od New i od Edit!
        public ActionResult Save(Customer customer) // ova moze i so UpdateCustomerDto vo Mapper
        {

            if (customer.Id == 0)
            { // znaci deka e nov i go dodavam vo Db
                _contex.Customers.Add(customer);

            }
            else
            {// ako Id != 0 ... znaci Id e vekje zafateno i daj da go najdeme toj User za da go updejtneme
               var oldCustomer = _contex.Customers.Single(c => c.Id == customer.Id);

                //TryUpdateModel(oldCustomer, "");
                //Mapper.Map(customer, oldCustomer); ako imav AutoMapper

                oldCustomer.Name = customer.Name;
                oldCustomer.BirthDay = customer.BirthDay;
                oldCustomer.MembershipTypeId = customer.MembershipTypeId;
                oldCustomer.IsSubscribedToNewsLetter = customer.IsSubscribedToNewsLetter;

            }


            //_contex.Customers.Add(customer); 
            _contex.SaveChanges();

            return RedirectToAction("Index", "Customers");
        }



        public ActionResult Edit(int Id)
        {
            var customer = _contex.Customers.SingleOrDefault(c => c.Id == Id);

            if (customer == null)
                return HttpNotFound();

            var ViewModel = new NewCustomerViewModel
            {
                Customer = customer,
                MembershipTypes = _contex.MembershipTypes.ToList()
            };

            // vrakjam polna klasa za da vrednostite se isti 
            return View("CustomerForm", ViewModel);
        } // CustomerForm te nosi vo Save



        // GET: Customers/Details/{id}
        public ActionResult Details(int id)
        {
            var movie = new Movie() { Name = "Shrek!" };

            var customer = _contex.Customers.Include(c=>c.MembershipType).SingleOrDefault(c => c.Id == id);


            if (customer != null)
            {
                return View(customer);

            };

            return Content("ups");

        }


       



    }
}