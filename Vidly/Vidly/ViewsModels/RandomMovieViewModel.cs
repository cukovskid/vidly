﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vidly.Models;

namespace Vidly.ViewsModel
{
    public class RandomMovieViewModel // modelot koj ke go koristam samo pri View // I mora vo VIEW preku nego!!
    {
        public Movie Movie { get; set; }
        public List<Customer> Customers { get; set; }
    }
}