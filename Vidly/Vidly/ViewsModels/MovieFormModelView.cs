﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vidly.Models;

namespace Vidly.ViewsModels
{
    public class MovieFormModelView
    {
        public IEnumerable<Genre> Genres { get; set; } // kako lista go prakjam za da mozam DropDown da napravam vo VIEW
        public Movie Movie { get; set; }
    }
}